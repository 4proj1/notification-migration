FROM bitnami/symfony

WORKDIR /app/project

CMD ["./bin/startup.sh"]

COPY . /app/project/
RUN chmod +x ./bin/startup.sh