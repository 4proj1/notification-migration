#!/bin/bash


# Parse database url
IFS='/'
read -ra ADDR <<< "$DATABASE_URL"

IFS='@'

read -ra ADDR <<< "${ADDR[2]}"

creds="${ADDR[0]}"
full_host="${ADDR[1]}"

IFS=':'

read -ra ADDR <<< "$creds"

username="${ADDR[0]}"
password="${ADDR[1]}"

read -ra ADDR <<< "$full_host"

hostname="${ADDR[0]}"
port="${ADDR[1]}"

IFS=' ' # reset to default value after usage

until mysql -h "$hostname" -u "$username" -p"$password" -P "$port"
do
    >&2 echo "Mariadb is unavailable - sleeping"
    sleep 1
done

>&2 echo "Mariadb is up"

php /app/project/bin/console m:m
php /app/project/bin/console doctrine:migrations:migrate